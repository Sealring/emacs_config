;; configuring the look and feel
;;
;; -*- lexical-binding: t -*-

(defun ecfg-gui-module-init ()
  "Entry function for ecfg init system."
  (ecfg--setup-basic-gui)
  (ecfg--setup-sml-modeline)
  (ecfg--setup-color-theme)
  (ecfg--setup-look-and-feel))


(defun ecfg--setup-basic-gui ()
;;; Disabling unused UI components
  (setq-default inhibit-startup-screen t)

  ;; we don't need menubar (execpt OSX), toolbar nor scrollbar
  (and (fboundp 'menu-bar-mode)
       (not (eq system-type 'darwin))
       (menu-bar-mode -1))
  (dolist (mode '(tool-bar-mode scroll-bar-mode))
    (when (fboundp mode) (funcall mode -1)))

;;; Scrolling
  ;; scroll two lines at a time (less "jumpy" than defaults but still snappy)
  (setq mouse-wheel-scroll-amount '(2 ((shift) . 1)))
  ;; don't accelerate scrolling
  (setq mouse-wheel-progressive-speed nil)
  ;; scroll window under mouse
  (setq mouse-wheel-follow-mouse 't)
  ;; The number of lines to try scrolling a window by when point moves out.
  ;; Actually I like the default behavior with re-centering of the screen
  ;; (setq scroll-step 1)

;;; Text-related UI
  (show-paren-mode t)
  (global-hl-line-mode t)
  (column-number-mode t))


(defun ecfg--setup-sml-modeline ()
  (ecfg-install sml-modeline
   (require 'sml-modeline)
   (setq sml-modeline-borders '("[" . "]"))
   (setq sml-modeline-len 14)
   (sml-modeline-mode t)))

(defun ecfg--setup-color-theme ()
  (add-to-list 'custom-theme-load-path (expand-file-name "themes" ecfg-dir))
  (setq x-underline-at-descent-line t)
  ;;(load-theme 'ecfg-light t)
  (load-theme 'smyx-jacob t))

(defun ecfg--setup-look-and-feel ()
  ;; transparency
  (set-frame-parameter (selected-frame) 'alpha 80)
  (defun toggle-transparency ()
    (interactive)
    (let ((alpha (frame-parameter nil 'alpha)))
      (set-frame-parameter
       nil 'alpha
       (if (eql (cond ((numberp alpha) alpha)
                      ((numberp (cdr alpha)) (cdr alpha))
                      ;; Also handle undocumented (<active> <inactive>) form.
                      ((numberp (cadr alpha)) (cadr alpha)))
                100)
           '(80 . 80) '(100 . 100)))))
  (global-set-key (kbd "s-=") 'toggle-transparency)
  (global-set-key (kbd "C-s-=") 'balance-windows)

  ;; change selection highlight colour:
  (set-face-attribute 'region nil :background "#003200")

  ;; select a font:
  (setq bdf-directory-list '("~/.emacs.config/fonts"))
  (set-face-attribute 'default nil :family "PT MONO" :height 130)

  ;; select a cursor style:
  (set-default 'cursor-type 'hbar)

  (custom-set-faces
   ;; custom-set-faces was added by Custom.
   ;; If you edit it by hand, you could mess it up, so be careful.
   ;; Your init file should contain only one such instance.
   ;; If there is more than one, they won't work right.
   '(fci-rule-color "#151515")
   '(dired-k-directory ((t (:foreground "cyan"))))
   '(ahs-definition-face ((t (:underline "coral" :weight extra-bold))))
   '(ahs-face ((t (:underline "yellow" :weight semi-bold))))
   '(ahs-plugin-defalt-face ((t (:weight bold))))
   '(ahs-plugin-whole-buffer-face ((t (:slant italic :weight bold))))
   '(company-tooltip-annotation ((t (:background "#333333" :foreground "light gray"))))
   '(fa-face-hint ((t (:background "#3f3f3f" :foreground "#ffffff"))))
   '(fa-face-hint-bold ((t (:background "#3f3f3f" :weight bold))))
   '(fa-face-semi ((t (:background "#3f3f3f" :foreground "#ffffff" :weight bold))))
   '(fa-face-type ((t (:inherit (quote font-lock-type-face) :background "#3f3f3f"))))
   '(fa-face-type-bold ((t (:inherit (quote font-lock-type-face) :background "#999999" :bold t))))
   '(flyspell-incorrect ((t (:underline (:color "dark orange" :style wave) :weight bold))))
   '(helm-buffer-directory ((t (:foreground "gold"))))
   '(helm-ff-directory ((t (:foreground "orange red"))))
   '(powerline-inactive1 ((t (:inherit nil :background "grey11" :foreground "dark gray"))))
   '(powerline-inactive2 ((t (:inherit nil :background "black" :foreground "dark gray"))))
   '(rscope-function-face ((t (:foreground "pale green"))))
   '(rscope-line-face ((t (:foreground "light sky blue"))))
   '(rscope-line-number-face ((t (:foreground "snow")))))

  (ecfg-install auto-highlight-symbol
                (require 'auto-highlight-symbol)
                (global-auto-highlight-symbol-mode t)
                (ahs-change-range 'ahs-range-whole-buffer)
                (diminish 'auto-highlight-symbol-mode))

  ;; line numbers:
  (global-linum-mode t)

  ;; powerline:
  (ecfg-install powerline
                (setq powerline-default-separator 'wave)
                (powerline-center-theme)))
