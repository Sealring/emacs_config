;; Defining global keybindings.
;;
;; Right now most of the global keybindings are defined here. Nevertheless, some
;; of the definitions still pop up in the module configurations (e.g. in
;; modules/ecfg-org.el). Please grep for `global-set-key' entries everywhere
;; before defining a new keybinding here.
;;
;; Mode- or buffer-local stuff is normally defined in hooks to corresponding
;; modes.
;;
;; -*- lexical-binding: t -*-

(defun ecfg-keybindings-module-init ()

;;; Compatibility

  ;; forcing emacs handle winkeys in windows
  (setq w32-pass-lwindow-to-system nil)
  (setq w32-pass-rwindow-to-system nil)
  (setq w32-lwindow-modifier 'super)
  (setq w32-rwindow-modifier 'super)

;;; Windows, files, and buffers

  ;; todo explore helm stuff
  (windmove-default-keybindings 'super)

  (global-set-key (kbd "C-s-b") 'ibuffer)
  (global-set-key (kbd "C-x C-b") 'ibuffer)
  (global-set-key (kbd "<C-s-268632066>") 'ibuffer) ;tweak for os-x



  (global-set-key (kbd "s-=") 'toggle-transparency)
  (global-set-key (kbd "C-c r") 'recentf-open-files)
  (global-set-key (kbd "C-<f11>") 'toggle-frame-fullscreen)
  (global-set-key (kbd "C-j") 'evil-join)
  ;; Winermode setups:
  (winner-mode t)
  (global-set-key (kbd "M-s-<left>") 'shrink-window-horizontally)
  (global-set-key (kbd "M-s-<right>") 'enlarge-window-horizontally)
  (global-set-key (kbd "M-s-<up>") 'shrink-window)
  (global-set-key (kbd "M-s-<down>") 'enlarge-window)
  (global-set-key (kbd "C-c C-c M-x") 'execute-extended-command)

  (ecfg-install ace-window
                (require 'ace-window)
                (global-set-key (kbd "C-x o") 'ace-window)
                ;; Sample of defaults for extra commands:
                ;; (defvar aw-dispatch-alist
                ;;   '((?x aw-delete-window "Delete Window")
                ;;     (?m aw-swap-window "Swap Windows")
                ;;     (?M aw-move-window "Move Window")
                ;;     (?j aw-switch-buffer-in-window "Seonlect Buffer")
                ;;     (?n aw-flip-window)
                ;;     (?u aw-switch-buffer-other-window "Switch Buffer Other Window")
                ;;     (?c aw-split-window-fair "Split Fair Window")
                ;;     (?v aw-split-window-vert "Split Vert Winodow")
                ;;     (?b aw-split-window-horz "Split Horz Window")
                ;;     (?o delete-other-windows "Delete Other Windows")
                ;;     (?? aw-show-dispatch-help))
                ;;   "List of actions for `aw-dispatch-default'.")
                )



  ;; (global-set-key (kbd "S-<right>" ) 'windmove-right)
  ;; (global-set-key (kbd "S-<left>") 'windmove-left)
  ;; (global-set-key (kbd "S-<down>" ) 'windmove-down)
  ;; (global-set-key (kbd "S-<up>") 'windmove-up)

  (global-unset-key (kbd "C-c <left>"))
  ;;(define-key ctl-x-4-map (kbd "u") 'winner-undo)
  (global-unset-key (kbd "C-v"))
  ;; Wiondow managment:
  (defun transpose-windows (arg)
    "Transpose the buffers (ARG) shown in two windows."
    (interactive "p")
    (let ((selector (if (>= arg 0) 'next-window 'previous-window)))
      (while (/= arg 0)
        (let ((this-win (window-buffer))
              (next-win (window-buffer (funcall selector))))
          (set-window-buffer (selected-window) next-win)
          (set-window-buffer (funcall selector) this-win)
          (select-window (funcall selector)))
        (setq arg (if (plusp arg) (1- arg) (1+ arg))))))

  (define-key ctl-x-4-map (kbd "t") 'transpose-windows)
  (define-key ctl-x-4-map (kbd "C-j") nil)
  (define-key ctl-x-4-map (kbd "d") 'dired-jump-other-window)

  ;; Kill dired bufferes:
  (defun kill-all-buffers ()
    ;;; Commentary:
    ;; allows you to kill all buffers
    (interactive)
    (mapc (lambda (buffer) (kill-buffer buffer))
          (buffer-list)))

  (global-set-key (kbd "C-c k") 'kill-all-buffers)
  (global-set-key (kbd "C-S-k") 'kill-whole-line)

  ;; window split toggle
  (defun window-toggle-split-direction ()
    "Switch window split from horizontally to vertically, or vice versa. i.e. change right window to bottom, or change bottom window to right."
    (interactive)
    (require 'windmove)
    (let ((done))
      (dolist (dirs '((right . down) (down . right)))
        (unless done
          (let* ((win (selected-window))
                 (nextdir (car dirs))
                 (neighbour-dir (cdr dirs))
                 (next-win (windmove-find-other-window nextdir win))
                 (neighbour1 (windmove-find-other-window neighbour-dir win))
                 (neighbour2 (if next-win (with-selected-window next-win
                                            (windmove-find-other-window neighbour-dir next-win)))))
            (message "win: %s\nnext-win: %s\nneighbour1: %s\nneighbour2:%s" win next-win neighbour1 neighbour2)
            (setq done (and (eq neighbour1 neighbour2)
                            (not (eq (minibuffer-window) next-win))))
            (if done
                (let* ((other-buf (window-buffer next-win)))
                  (delete-window next-win)
                  (if (eq nextdir 'right)
                      (split-window-vertically)
                    (split-window-horizontally))
                  (set-window-buffer (windmove-find-other-window neighbour-dir) other-buf))))))))
  (define-key ctl-x-4-map (kbd "`") 'window-toggle-split-direction)

;;; Copy, paste, insert
  (global-set-key [?\C-z] 'undo)
  (global-set-key (kbd "s-z") 'redo)

  ;; TODO kill whole word or selection instead
  (global-set-key (kbd "C-w") 'ecfg-kill-word-or-selection)
  (global-set-key (kbd "C-S-w") 'ecfg-kill-line-or-selection)
  (global-set-key (kbd "M-w") 'ecfg-copy-word-or-selection)
  (global-set-key (kbd "M-S-w") 'ecfg-copy-line-or-selection)
  (global-set-key (kbd "C-y") 'clipboard-yank)
  (defun jacob/yank-with-pop ()
    (interactive)
    (yank)
    (yank-pop)
    (pop kill-ring))
  (global-set-key (kbd "C-S-y") 'jacob/yank-with-pop)
  (global-set-key "\M-Y" 'yank-pop)
  (global-set-key "\C-\M-y" 'helm-show-kill-ring)
  (global-set-key (kbd "M-s-m") 'mc/mark-pop)

  (global-set-key (kbd "s-i") 'quoted-insert)
  (global-set-key (kbd "s-!") 'eval-expression)

  (global-set-key (kbd "M-;") nil)
  (global-set-key (kbd "M-d") 'duplicate-current-line-or-region)
  (global-set-key (kbd "C-/") 'ecfg-toggle-comment-on-lines)
  (global-set-key (kbd "C-M-/") 'comment-indent)


;;; Indentation, line manipulation

  (global-set-key (kbd "s-j") 'delete-indentation)
  (global-set-key (kbd "s-n") 'goto-line)
  (global-set-key (kbd "s-k") 'ecfg-kill-current-line)
  (global-set-key (kbd "<S-return>") 'ecfg-end-of-line-and-indent)
  (global-set-key [C-S-up] 'drag-stuff-up)
  (global-set-key [C-S-down] 'drag-stuff-down)

  (global-set-key (kbd "\C-c i") 'ecfg-reformat-text)


;;; Search and replace
  (global-set-key (kbd "s-r") 'query-replace-regexp)

  (global-set-key (kbd "s-h") 'ecfg-highlight-current-tag-occurences)
  (global-set-key (kbd "C-S-s-h") 'ecfg-highlight-occurences)
  (global-set-key (kbd "C-s-h") 'ecfg-unhighlight-all)
  (global-set-key (kbd "<C-s-268632072>") 'ecfg-unhighlight-all) ;tweak for os-x

  (global-set-key (kbd "C-c o") 'occur)

  ;; insert new line before current:
  (defun insert-before-line ()
    "Insert a newline above the current line and put point at beginning."
    (interactive)
    (unless (bolp)
      (beginning-of-line))
    (newline)
    (forward-line -1)
    (indent-according-to-mode))
  (global-set-key (kbd "<S-M-return>") 'insert-before-line)

;;; Fn keys

  (global-set-key [f5] 'bookmark-set)
  (global-set-key [f6] 'bookmark-jump)


;;; Completion

  ;; hippie expand is dabbrev expand on steroids
  (global-set-key (kbd "M-/") 'hippie-expand)
  (global-set-key (kbd "<C-tab>") 'company-complete)


;;; Font size

  (define-key global-map (kbd "C-+") 'text-scale-increase)
  (define-key global-map (kbd "C-_") 'text-scale-decrease)


;;; Help and reference

  (global-set-key (kbd "C-h a") 'apropos)


;;; Unbinding and aborting

  (global-set-key (kbd "<escape>") 'keyboard-quit)
  ;; (global-set-key (kbd "C-q") 'keyboard-quit)
  (define-key minibuffer-local-map (kbd "<escape>") 'minibuffer-keyboard-quit)
  (define-key minibuffer-local-map (kbd "C-q") 'minibuffer-keyboard-quit)

  (global-unset-key (kbd "s-m"))        ;on mac runs iconify-frame
  (global-unset-key (kbd "s-q"))        ;on mac runs kill-emacs
  (global-unset-key (kbd "s-t"))        ;os-x assigns ns-popup-font-panel for it
  (global-unset-key (kbd "C-x C-z"))    ;don't need to minimize it with shortcut


;;; Kill buffer hook

  ;; Providing a universal keybinding for ending the work with buffer for both
  ;; standard and client modes
  (add-hook 'server-switch-hook (lambda ()
                                  (local-set-key (kbd "C-x k") '(lambda ()
                                                                  (interactive)
                                                                  (if server-buffer-clients (server-edit) (ido-kill-buffer))))))


;;; Overriding annoying defaults and undef if needed
  (global-set-key (kbd "C-x C-f") 'helm-find-files)
  (global-set-key (kbd "C-x f") 'helm-find-files)
  (global-set-key (kbd "M-t") nil)
  (global-set-key (kbd "C-d") nil)
  (global-set-key (kbd "s-e") 'electric-indent-mode))
