(defun ecfg-navigation-module-init ()
  (ecfg--setup-fancy-narrow)
  (ecfg--setup-mc)
  (ecfg--setup-ace-navigation)
  (ecfg--setup-expand-region)
  (ecfg--setup-matching-paren-jump)
  (ecfg--setup-custom-dired)
  (ecfg--setup-pop-mark))

(defun ecfg--setup-ace-navigation ()

  ;; #TODO reuse thisselectioon function for C-w/M-w
  (defun xah-search-current-word ()
    "Call `isearch' on current word or text selection.
“word” here is A to Z, a to z, and hyphen 「-」 and underline 「_」, independent of syntax table.
URL `http://ergoemacs.org/emacs/modernization_isearch.html'
Version 2015-04-09"
    (interactive)
    (let ( $p1 $p2 )
      (if (use-region-p)
          (progn
            (setq $p1 (region-beginning))
            (setq $p2 (region-end)))
        (save-excursion
          (skip-chars-backward "-_A-Za-z0-9")
          (setq $p1 (point))
          (right-char)
          (skip-chars-forward "-_A-Za-z0-9")
          (setq $p2 (point))))
      (setq mark-active nil)
      (when (< $p1 (point))
        (goto-char $p1))
      (isearch-mode t)
      (isearch-yank-string (buffer-substring-no-properties $p1 $p2))))

  (global-set-key (kbd "C-s") 'xah-search-current-word)
  ;; setup dired ace jump:
  (setq dired-isearch-filenames t)
  (add-hook 'dired-mode-hook
            (lambda ()
              (setq-local ace-jump-search-filter
                          (lambda ()
                            (get-text-property (point) 'dired-filename)))))

  (ecfg-install avy
                (require 'avy)
                (avy-setup-default)
                ;; #TODO implement this func
                ;; (defun jacob/avy-goto-char-in-line-if-exists ()
                ;;   (interactive (list (read-char "char: " t)))
                ;;   (avy-goto-char-in-line 'list)
                ;;   )
                (global-set-key (kbd "M-J") 'avy-goto-char)
                (global-set-key (kbd "M-j") 'avy-goto-char-in-line)
                (global-set-key (kbd "M-l") 'avy-goto-line)
                (global-set-key (kbd "M-s-j") 'avy-goto-word-1)
                (global-set-key (kbd "C-M-'") 'avy-pop-mark)
                (global-set-key (kbd "C-M-#") 'avy-pu)
                (global-set-key (kbd "C-M-j") 'avy-goto-char-timer))

  ;; something is broken, #TODO
  ;; (ecfg-install ace-link
  ;;               (require 'ace-link)
  ;;               (ace-link-setup-default)
  ;;               (defun avy-jump-error-next-error-hook ()
  ;;                 (let ((compilation-buffer (compilation-find-buffer t)))
  ;;                   (quit-window nil (get-buffer-window compilation-buffer))
  ;;                   (recenter)))
  ;;               (defun avy-jump-error()
  ;;                    (let ((compilation-buffer (compilation-find-buffer t))
  ;;                          (next-error-hook '(avy-jump-error-next-error-hook)))
  ;;                      (when compilation-buffer
  ;;                        (with-current-buffer compilation-buffer
  ;;                          (when (derived-mode-p 'compilation-mode)
  ;;                            (pop-to-buffer compilation-buffer)
  ;;                            (ace-link-compilation))))))
  ;;               (global-set-key (kbd "M-g e") 'avy-jump-error))

  (ecfg-install ace-jump-zap
                (require 'ace-jump-zap)
                (global-set-key (kbd "M-z") 'ace-jump-zap-to-char))

  (setq ajz/zap-function 'kill-region)

  ;; #TODO evaluate is needed:
  ;; (ecfg-install ace-window
  ;;              :ensure t
  ;;              :bind (([remap next-multiframe-window] . ace-window))
  ;;              :config
  ;;              (setq aw-keys '(?a ?s ?d ?f ?g ?h ?j ?k ?l)))
  )
(defun ecfg--setup-expand-region()
  (ecfg-install expand-region
                (require 'expand-region)
                (global-set-key (kbd "C-S-<right>") 'er/expand-region)
                (global-set-key (kbd "C-S-<left>") 'er/contract-region)))

(defun ecfg--setup-fancy-narrow()
  (ecfg-install fancy-narrow
                (require 'fancy-narrow)
                (global-set-key (kbd "C-x n d") 'fancy-narrow-to-defun)
                (global-set-key (kbd "C-x n p") 'fancy-narrow-to-page)
                (global-set-key (kbd "C-x n n") 'fancy-narrow-to-region)
                (global-set-key (kbd "C-x n w") 'fancy-widen)))

(defun ecfg--setup-mc()
  (ecfg-install multiple-cursors
                (require 'multiple-cursors)
                (global-set-key (kbd "M-<up>") 'mc/mark-previous-like-this)
                (global-set-key (kbd "M-<down>") 'mc/mark-next-like-this)
                (global-set-key (kbd "M-]") 'mc/mark-all-like-this-dwim)
                (global-set-key (kbd "M-[") 'mc/edit-lines)
                (add-hook 'before-save-hook #'mc/keyboard-quit)))

(defun ecfg--setup-matching-paren-jump()
  (defun goto-match-paren (arg)
    "Go to the matching parenthesis if on parenthesis. Else go to the
   opening parenthesis one level up."
    (interactive "p")
    (cond ((looking-at "\\s\(") (forward-list 1))
          (t
           (backward-char 1)
           (cond ((looking-at "\\s\)")
                  (forward-char 1) (backward-list 1))
                 (t
                  (while (not (looking-at "\\s("))
                    (backward-char 1)
                    (cond ((looking-at "\\s\)")
                           (message "->> )")
                           (forward-char 1)
                           (backward-list 1)
                           (backward-char 1)))
                    ))))))
  (global-set-key (kbd "C-p") 'goto-match-paren))

(defun ecfg--setup-custom-dired ()
  (ecfg-install dired-k
                (require 'dired-k)
                (define-key dired-mode-map (kbd "K") 'dired-k)

                ;; You can use dired-k alternative to revert-buffer
                (define-key dired-mode-map (kbd "g") 'dired-k)

                ;; always execute dired-k when dired buffer is opened
                (add-hook 'dired-initial-position-hook 'dired-k)

                (add-hook 'dired-after-readin-hook #'dired-k-no-revert)))

(defun ecfg--setup-pop-mark ()
    (defface pop-mark-face-foreground-presistent
      '((((class color)) (:foreground "red" :underline nil :bold t :underline t))
        (((background dark)) (:foreground "gray100" :underline nil))
        (((background light)) (:foreground "gray0" :underline nil))
        (t (:foreground "gray100" :underline nil)))
      "Face for foreground of MarkPop marks that are persistant"
      :group 'pop-mark)

    (defface pop-mark-face-foreground
      '((((class color)) (:foreground "red" :underline nil))
        (((background dark)) (:foreground "gray100" :underline nil))
        (((background light)) (:foreground "gray0" :underline nil))
        (t (:foreground "gray100" :underline nil)))
      "Face for foreground of MarkPop marks"
      :group 'pop-mark)

    (setq pop-mark-marks-list ())
    (make-variable-buffer-local 'pop-mark-marks-list)

    (defun pop-mark-push-or-pop-position (mark-char)
      "Adds or Removes a mark from the list of known marks"
      (interactive (list (read-char "Mark Char:")))
      (pop-mark-do-mark-or-pop mark-char nil))

    (defun pop-mark-push-or-pop-position-presistent (mark-char)
      "Adds or Removes a mark from the list of known marks but keeps them presistent"
      (interactive (list (read-char "Mark Char:")))
      (pop-mark-do-mark-or-pop mark-char t))

    (defun pop-mark-do-mark-or-pop (mark-char presistent)
      (let ((mp (assoc mark-char pop-mark-marks-list)))
        (message "%c" mark-char)
        (if mp
            (let ()
              (message "going to pop-mark %c" mark-char)
              (if (overlayp (cadr mp))
                  (let ()
                    (goto-char (overlay-start (cadr mp)))
                    (if (or (not (caddr mp)) presistent)
                        (let ()
                          (message "removing pop-mark %c" mark-char)
                          (delete-overlay (cadr mp))
                          (setf pop-mark-marks-list (delete mp pop-mark-marks-list)))))
                (setf pop-mark-marks-list  (delete mp pop-mark-marks-list))))
          (let* ((ol (make-overlay (point) (+ (point) 1))))
            (message "adding %c" mark-char)
            (add-to-list 'pop-mark-marks-list (list mark-char ol presistent))
            (if presistent
                (overlay-put ol 'face 'pop-mark-face-foreground-presistent)
              (overlay-put ol 'face 'pop-mark-face-foreground))
            (overlay-put ol 'display
                         (cond ((string-equal (buffer-substring (point) (+ 1 (point))) "\n") (concat (char-to-string mark-char) "\n"))
                               ((string-equal (buffer-substring (point) (+ 1 (point))) "\t") (concat (char-to-string mark-char) "\t"))
                               (t (char-to-string mark-char))))))))

    (defun pop-mark-kill-all-marks ()
      "Removes all pop-marks"
      (interactive)
      (cl-flet ((remover (lambda (mp)
                        (delete-overlay (cadr mp)))))
        (mapcar #'remover pop-mark-marks-list)
        (setf pop-mark-marks-list ())))

    (global-set-key (kbd "M-m") 'pop-mark-push-or-pop-position)
    (global-set-key (kbd "M-M") 'pop-mark-push-or-pop-position-presistent)
    (global-set-key (kbd "C-M-m") 'pop-mark-kill-all-marks)
    (global-unset-key (kbd "C-x C-d"))
    (global-set-key (kbd "C-x C-d") 'dired))
