(defun ecfg-git-module-init ()
  (ecfg--setup-magit))


(defun ecfg--setup-magit()
  ;;(ecfg-install helm-gitignore)

  ;; highlight changes
   (ecfg-install git-gutter
                 (require 'git-gutter)
                 (diminish 'git-gutter-mode)
                 (global-git-gutter-mode)
                 (git-gutter:linum-setup))

   ;; magit
   ;; (if (eq system-type 'windows-nt)
   ;;     (progn
   ;;       (require 'package)
   ;;       (add-to-list 'package-archives
   ;;                    '("melpa-stable" . "http://stable.melpa.org/packages/") t)
   ;;       (package-refresh-contents)
   ;;       (package-install 'magit)
   ;;       (add-to-list 'load-path "~/.emacs.d/el-get/magit/lisp")
   ;;       (require 'magit)
   ;;       (add-to-list 'magit-log-arguments "--no-abbrev-commit")

   ;;       (require 'helm-mode)
   ;;       (setq magit-popup-use-prefix-argument 'default
   ;;             magit-completing-read-function 'helm--completing-read-default)
   ;;       (with-eval-after-load 'info
   ;;         (info-initialize)
   ;;         (add-to-list 'Info-directory-list
   ;;                      "~/.emacs.d/site-lisp/magit/Documentation/"))
   ;;       (global-git-commit-mode)
   ;;       (global-set-key (kbd "C-c m s") 'magit-status)
   ;;       (global-set-key (kbd "C-c m b") 'magit-branch)
   ;;       (global-set-key (kbd "C-c m p") 'magit-push)
   ;;       (global-set-key (kbd "C-c m o") 'magit-pull)
   ;;       (global-set-key (kbd "C-c m l") 'magit-log)
   ;;       (global-set-key (kbd "C-c m c") 'magit-checkout)
   ;;       (global-set-key (kbd "C-c m d") 'magit-diff--dwim))
   ;;   (progn
       (ecfg-install dash
                     (require 'dash))
       (ecfg-install with-editor
                     (require 'with-editor))
       (ecfg-install magit
                     ;; #TODO set it up to be autoloaded on magit sttus projectile-vc call
                     (add-to-list 'load-path "~/.emacs.d/el-get/magit/lisp")
                     (require 'magit)
                     (ecfg-install evil-magit)
                     (add-to-list 'magit-log-arguments "--no-abbrev-commit")

                     (require 'helm-mode)
                     (setq magit-popup-use-prefix-argument 'default
                           magit-completing-read-function 'helm--completing-read-default)
                     (with-eval-after-load 'info
                       (info-initialize)
                       (add-to-list 'Info-directory-list
                                    "~/.emacs.d/site-lisp/magit/Documentation/"))
                     (global-git-commit-mode)
                     (global-set-key (kbd "C-c m s") 'magit-status)
                     (global-set-key (kbd "C-c m b") 'magit-branch)
                     (global-set-key (kbd "C-c m p") 'magit-push)
                     (global-set-key (kbd "C-c m o") 'magit-pull)
                     (global-set-key (kbd "C-c m l") 'magit-log)
                     (global-set-key (kbd "C-c m c") 'magit-checkout)
                     (global-set-key (kbd "C-c m d") 'magit-diff--dwim)))
;; ))
