;; -*- lexical-binding: t -*-

;;;###autoload
(defun ecfg-ruby-module-init ()
  (add-hook 'ruby-mode-hook 'ruby-refactor-mode-launch)
  (ecfg-install ruby-refactor
                (add-hook 'ruby-mode-hook 'ruby-refactor-mode-launch))
  (ecfg-install inf-ruby
                (autoload 'inf-ruby "inf-ruby" nil t))
  (ecfg-install robe
                (require 'robe)
                (autoload 'robe "robe" nil t)
                (add-hook 'ruby-mode-hook 'robe-mode))
  ;; (inf-ruby-console-auto)
  )

;;;###autoload (ecfg-auto-module "\\.\\(?:cap\\|gemspec\\|irbrc\\|gemrc\\|rake\\|rb\\|ru\\|thor\\)\\'" ruby)
;;;###autoload (ecfg-auto-module "\\.rb$" ruby)
;;;###autoload (ecfg-auto-module "\\.\\(?:cap\\|gemspec\\|irbrc\\|gemrc\\|rake\\|rb\\|ru\\|thor\\)\\'" ruby)
