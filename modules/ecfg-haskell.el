;; -*- lexical-binding: t -*-

;;;###autoload
(defun ecfg-haskell-module-init ()
 (message "Reverting `%s'..." (buffer-name))
 (sit-for 1)
 (message "done")
 (ecfg-install haskell-mode
               (require 'haskell-mode))
 (ecfg-install haskell-snippets)
               ;(require 'haskell-snippets))
 (ecfg-install haskell-tab-indent)
;; (ecfg-install hasky-extentions)
 (ecfg-install flycheck-haskell)
 (ecfg-install ghc-mod)
 (ecfg-install ghc-imported-from))

;;;###autoload (ecfg-auto-module "\\.hs$" haskell)
