(defvar topp/saved-points-alist nil
  "Internal variable for caching the set of set points.")

(make-variable-buffer-local 'topp/saved-points-alist)

(assq '1 topp/saved-points-alist)

(defun topp/tag-or-pop-point ()
  (interactive)
  (if (eq (assq '1 topp/saved-points-alist) nil)
      (topp/do--tag)
    (topp/do--pop)))

(defun topp/do--pop ()
  (assq-delete-all 1 topp/saved-points-alist)
  (message "poped"))

(defun topp/do--tag ()
  (add-to-list 'topp/saved-points-alist '(1 . ((point) (make-overlay o ))))
  (message "tagged"))
